package fr.ipazu.advancedrealm.events;

import fr.ipazu.advancedrealm.realm.Realm;
import fr.ipazu.advancedrealm.realm.RealmPlayer;
import org.bukkit.event.Cancellable;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

public class RealmClaimEvent extends Event implements Cancellable {
    private static final HandlerList handlers = new HandlerList();

    private final RealmPlayer realmPlayer;
    private final Realm realm;
    private boolean cancelled = false;

    public RealmClaimEvent(Realm realm, RealmPlayer realmPlayer) {
        this.realm = realm;
        this.realmPlayer = realmPlayer;
    }

    @Override
    public boolean isCancelled() {
        return cancelled;
    }

    @Override
    public void setCancelled(boolean b) {
      this.cancelled = b;
    }

    @Override
    public HandlerList getHandlers() {
        return handlers;
    }
    public Realm getRealm(){
        return realm;
    }
    public RealmPlayer getRealmPlayer(){
        return realmPlayer;
    }

}
