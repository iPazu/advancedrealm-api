package fr.ipazu.advancedrealm.events;

import fr.ipazu.advancedrealm.realm.Realm;
import fr.ipazu.advancedrealm.realm.RealmPlayer;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.Cancellable;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

public class RealmBuildEvent extends Event implements Cancellable {
    private static final HandlerList handlers = new HandlerList();

    private final RealmPlayer realmPlayer;
    private final Realm realm;
    private final Player player;
    private final Block block;

    private boolean cancelled = false;

    public RealmBuildEvent(Realm realm, RealmPlayer realmPlayer, Player player, Block block) {
        this.realm = realm;
        this.realmPlayer = realmPlayer;
        this.player = player;
        this.block = block;
    }

    @Override
    public boolean isCancelled() {
        return cancelled;
    }

    @Override
    public void setCancelled(boolean b) {
        this.cancelled = b;
    }

    @Override
    public HandlerList getHandlers() {
        return handlers;
    }
    public Realm getRealm(){
        return realm;
    }
    public RealmPlayer getRealmPlayer(){
        return realmPlayer;
    }
    public Player getPlayer(){
        return player;
    }
    public Block getBlock(){
        return block;
    }
}
