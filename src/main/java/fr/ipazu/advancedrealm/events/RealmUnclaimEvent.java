package fr.ipazu.advancedrealm.events;

import fr.ipazu.advancedrealm.realm.Realm;
import fr.ipazu.advancedrealm.realm.RealmPlayer;
import org.bukkit.entity.Player;
import org.bukkit.event.Cancellable;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

public class RealmUnclaimEvent extends Event implements Cancellable {
    private static final HandlerList handlers = new HandlerList();

    private final RealmPlayer realmPlayer;
    private final Realm realm;
    private final Player player;
    private boolean cancelled = false;

    public RealmUnclaimEvent(Realm realm, RealmPlayer realmPlayer, Player player) {
        this.realm = realm;
        this.realmPlayer = realmPlayer;
        this.player = player;
    }

    @Override
    public boolean isCancelled() {
        return cancelled;
    }

    @Override
    public void setCancelled(boolean b) {
        this.cancelled = b;
    }

    @Override
    public HandlerList getHandlers() {
        return handlers;
    }
    public Realm getRealmUnclaimed(){
        return realm;
    }
    public RealmPlayer getRealmPlayer(){
        return realmPlayer;
    }
    public Player getPlayer(){
        return player;
    }
}
